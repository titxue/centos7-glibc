# 选择运行时基础镜像
FROM centos:7

# 维护人员
MAINTAINER 徐晓伟 xuxiaowei@xuxiaowei.com.cn

RUN ldd --version || echo 不存在ldd

ENV MAKE_VERSION=4.4
ENV GLIBC_VERSION=2.37

WORKDIR /root

ADD glibc-${GLIBC_VERSION}.tar.gz .
ADD make-4.4.tar.gz .
RUN ls

WORKDIR /root/make-4.4
RUN yum -y install make
RUN yum -y install gcc
RUN ./configure --disable-dependency-tracking
RUN make
RUN make install
RUN make -v
RUN yum -y remove make
RUN make -v

WORKDIR /root/glibc-${GLIBC_VERSION}/build
RUN pwd
RUN ls

RUN yum -y install gcc-c++

RUN yum -y install bison
RUN yum -y install python3
RUN ../configure
RUN make
RUN make install

RUN ldd --version

WORKDIR /root

RUN rm -rf /root/glibc-${GLIBC_VERSION}
